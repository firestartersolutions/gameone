package gui;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import app.Game;
import gui.Buttons.*;
import gui.Buttons.MenuItems.MainMenu;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Toolkit;

public class Window {
	
	ButtonPanel bp = new ButtonPanel();
	
    public void start() {
    	MainMenu menu = new MainMenu();
    	java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize(); // �������� ������� ������
    	initGL((int)screenSize.getWidth()*65/100,(int)screenSize.getHeight()*78/100); // ���������� ����������� ������ � ���� ����
    	
        menu.initTextures(); // ������������� �������
        menu.initButtons(); // ������������� ������
        
        Game.init();
        
    	while(bp.pr) {	
    		menu.handleInput(); // �������� �� ������� ������
    		
	        menu.render(); // ��������� �������
	        
	        Display.update(); // ���������� ������ 
	        
	        try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
    	}
    	Display.destroy();
	    System.exit(0);
    }

    public void initGL(int width, int height) {
        try {
            Display.setDisplayMode(new DisplayMode(width,height)); // ������ ������ � ������ ������
            Display.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
            System.exit(0);
        }
        
        Display.setTitle("CatBan");
        
        glEnable(GL_TEXTURE_2D);
        glShadeModel(GL_SMOOTH);  
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_LIGHTING);
        glClearColor(1.000f, 0.855f, 0.725f, 0.0f);      
        glClearDepth(1);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glViewport(0,0,width,height);
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, width, height, 0, 1, -1);
        glMatrixMode(GL_MODELVIEW);
    }
}
