package gui.TexturesPack;

import java.io.Serializable;

import org.newdawn.slick.opengl.Texture;

import gui.TexturesPack.TexturesLoader;

public class Textures implements Serializable {
	
	private static final long serialVersionUID = 1516927209913354758L;
	private transient Texture backgroundTexture;
	private transient Texture playerTexture;
	private transient Texture ballTexture;
	private transient Texture alternateBoxTexture;
	private transient Texture wallTexture;
	private transient Texture floorTexture;
	private transient Texture endZoneTexture;
	private transient Texture congratsTexture;
	private transient Texture passingTexture;
	private transient Texture goodPassingTexture;
	private transient Texture perfectPassingTexture;
	private transient Texture spiralBackgroundTexture;
	private transient Texture stepsTexture;
	
	private String pathBackgroundTexture;
	private String pathPlayerTexture;
	private String pathBallTexture;
	private String pathAlternateBoxTexture;
	private String pathWallTexture;
	private String pathFloorTexture;
	private String pathEndZoneTexture;
	private String pathCongratsTexture;
	private String pathPassingTexture;
	private String pathGoodPassingTexture;
	private String pathPerfectPassingTexture;
	private String pathSpiralBackgroundTexture;
	private String pathStepsTexture;
	
	public Textures () {
		
	}
	
	public Textures(String pathBackgroundTexture, String pathPlayerTexture, String pathBallTexture, 
			String pathAlternateBoxTexture, String pathWallTexture, String pathFloorTexture, String pathEndZoneTexture, String pathCongratsTexture,
			String pathPassingTexture, String pathGoodPassingTexture, String pathPerfectPassingTexture,
			String pathSpiralBackgroundTexture, String pathStepsTexture) {
		
		this.pathBackgroundTexture = pathBackgroundTexture;
		this.pathPlayerTexture = pathPlayerTexture;
		this.pathBallTexture = pathBallTexture;
		this.pathAlternateBoxTexture = pathAlternateBoxTexture;
		this.pathWallTexture = pathWallTexture;
		this.pathFloorTexture = pathFloorTexture;
		this.pathEndZoneTexture = pathEndZoneTexture;
		this.pathCongratsTexture = pathCongratsTexture;
		this.pathPassingTexture = pathPassingTexture;
		this.pathGoodPassingTexture = pathGoodPassingTexture;
		this.pathPerfectPassingTexture = pathPerfectPassingTexture;
		this.pathSpiralBackgroundTexture = pathSpiralBackgroundTexture;
		this.pathStepsTexture = pathStepsTexture;
		
		backgroundTexture = TexturesLoader.loadTexture(pathBackgroundTexture);
		playerTexture = TexturesLoader.loadTexture(pathPlayerTexture);
		ballTexture = TexturesLoader.loadTexture(pathBallTexture);
		alternateBoxTexture = TexturesLoader.loadTexture(pathAlternateBoxTexture);
		wallTexture = TexturesLoader.loadTexture(pathWallTexture);
		floorTexture = TexturesLoader.loadTexture(pathFloorTexture);
		endZoneTexture = TexturesLoader.loadTexture(pathEndZoneTexture);
		congratsTexture = TexturesLoader.loadTexture(pathCongratsTexture);
		passingTexture = TexturesLoader.loadTexture(pathPassingTexture);
		goodPassingTexture = TexturesLoader.loadTexture(pathGoodPassingTexture);
		perfectPassingTexture = TexturesLoader.loadTexture(pathPerfectPassingTexture);
		spiralBackgroundTexture = TexturesLoader.loadTexture(pathSpiralBackgroundTexture);
		stepsTexture = TexturesLoader.loadTexture(pathStepsTexture);
	}
	
	public void pathToTextures() {
		backgroundTexture = TexturesLoader.loadTexture(pathBackgroundTexture);
		playerTexture = TexturesLoader.loadTexture(pathPlayerTexture);
		ballTexture = TexturesLoader.loadTexture(pathBallTexture);
		alternateBoxTexture = TexturesLoader.loadTexture(pathAlternateBoxTexture);
		wallTexture = TexturesLoader.loadTexture(pathWallTexture);
		floorTexture = TexturesLoader.loadTexture(pathFloorTexture);
		endZoneTexture = TexturesLoader.loadTexture(pathEndZoneTexture);
		congratsTexture = TexturesLoader.loadTexture(pathCongratsTexture);
		passingTexture = TexturesLoader.loadTexture(pathPassingTexture);
		goodPassingTexture = TexturesLoader.loadTexture(pathGoodPassingTexture);
		perfectPassingTexture = TexturesLoader.loadTexture(pathPerfectPassingTexture);
		spiralBackgroundTexture = TexturesLoader.loadTexture(pathSpiralBackgroundTexture);
		stepsTexture = TexturesLoader.loadTexture(pathStepsTexture);
	}
	
	public void setBackgroundTexture(String pathBackgroundTexture) {
		backgroundTexture = TexturesLoader.loadTexture(pathBackgroundTexture);
	}
	
	public Texture getBackgroundTexture() {
		return backgroundTexture;
	}
	
	public void setPlayerTexture(String pathPlayerTexture) {
		playerTexture = TexturesLoader.loadTexture(pathPlayerTexture);
	}
	
	public Texture getPlayerTexture() {
		return playerTexture;
	}
	
	public void setBallTexture(String pathBallTexture) {
		ballTexture = TexturesLoader.loadTexture(pathBallTexture);
	}
	
	public Texture getBallTexture() {
		return ballTexture;
	}
	
	public void setAlternateBoxTexture(String pathAlternateBoxTexture) {
		alternateBoxTexture = TexturesLoader.loadTexture(pathAlternateBoxTexture);
	}
	
	public Texture getAlternateBoxTexture() {
		return alternateBoxTexture;
	}
	
	public void setWallTexture(String pathWallTexture) {
		wallTexture = TexturesLoader.loadTexture(pathWallTexture);
	}

	public Texture getWallTexture() {
		return wallTexture;
	}
	
	public void setFloorTexture(String pathFloorTexture) {
		floorTexture = TexturesLoader.loadTexture(pathFloorTexture);
	}
	
	public Texture getFloorTexture() {
		return floorTexture;
	}
	
	public void setEndZoneTexture(String pathEndZoneTexture) {
		endZoneTexture = TexturesLoader.loadTexture(pathEndZoneTexture);
	}
	
	public Texture getEndZoneTexture() {
		return endZoneTexture;
	}
	
	public void setCongratsTexture(String pathCongratsTexture) {
		congratsTexture = TexturesLoader.loadTexture(pathCongratsTexture);
	}
	
	public Texture getCongratsTexture() {
		return congratsTexture;
	}
	
	public void setPassingTexture(String pathPassingTexture) {
		passingTexture = TexturesLoader.loadTexture(pathPassingTexture);
	}
	
	public Texture getPassingTexture() {
		return passingTexture;
	}
	
	public void setGoodPassingTexture(String pathGoodPassingTexture) {
		goodPassingTexture = TexturesLoader.loadTexture(pathGoodPassingTexture);
	}
	
	public Texture getGoodPassingTexture() {
		return goodPassingTexture;
	}
	
	public void setPerfectPassingTexture(String pathPerfectPassingTexture) {
		perfectPassingTexture = TexturesLoader.loadTexture(pathPerfectPassingTexture);
	}
	
	public Texture getPerfectPassingTexture() {
		return perfectPassingTexture;
	}
	
	public void setSpiralBackgroundTexture(String pathSpiralBackgroundTexture) {
		spiralBackgroundTexture = TexturesLoader.loadTexture(pathSpiralBackgroundTexture);
	}
	
	public Texture getSpiralBackgroundTexture() {
		return spiralBackgroundTexture;
	}
	
	public void setStepsTexture(String pathStepsTexture) {
		stepsTexture = TexturesLoader.loadTexture(pathStepsTexture);
	}
	
	public Texture getStepsTexture() {
		return stepsTexture;
	}
	
	public void setPathPlayerTexture(String pathPlayerTexture) {
		this.pathPlayerTexture = pathPlayerTexture;
	}
	
	public String getPathPlayerTexture() {
		return this.pathPlayerTexture;
	}
			
	public void setPathBallTexture(String pathBallTexture) {
		this.pathBallTexture = pathBallTexture;
	}
	
	public String getPathBallTexture() {
		return this.pathBallTexture;
	}
	
	public void setPathAlternateBoxTexture(String pathAlternateBoxTexture) {
		this.pathAlternateBoxTexture = pathAlternateBoxTexture;
	}
	
	public String getPathAlternateBoxTexture() {
		return this.pathAlternateBoxTexture;
	}
}
