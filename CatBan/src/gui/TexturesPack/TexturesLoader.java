package gui.TexturesPack;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Point;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

public class TexturesLoader {
	// ��������� �������
	public static Texture loadTexture(String pathTexture) {
		try {
			return TextureLoader.getTexture("PNG", new FileInputStream(new File(pathTexture)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	// ��������� ������� �� �����������
	public static void textureRenderXYCoordinates(Texture tex, int upBoard, int LeftBoard) { 
		tex.bind();
		
		GL11.glBegin(GL11.GL_QUADS);
	        GL11.glTexCoord2f(0, 0);
	        GL11.glVertex2f(upBoard, LeftBoard);
	        GL11.glTexCoord2f(1, 0);
	        GL11.glVertex2f(tex.getTextureWidth() + upBoard, LeftBoard);
	        GL11.glTexCoord2f(1, 1);
	        GL11.glVertex2f(tex.getTextureWidth() + upBoard, tex.getTextureHeight() + LeftBoard);
	        GL11.glTexCoord2f(0, 1);
	        GL11.glVertex2f(upBoard, tex.getTextureHeight() + LeftBoard);
        GL11.glEnd();
	}
	
	// ��������� ������� �� �����
	public static void textureRenderPoint(Texture tex, Point pos) { 
		tex.bind();
		
		GL11.glBegin(GL11.GL_QUADS);
	        GL11.glTexCoord2f(0, 0);
	        GL11.glVertex2f(pos.getX(),pos.getY());
	        GL11.glTexCoord2f(1, 0);
	        GL11.glVertex2f(tex.getTextureWidth() + pos.getX(), pos.getY());
	        GL11.glTexCoord2f(1, 1);
	        GL11.glVertex2f(tex.getTextureWidth() + pos.getX(), tex.getTextureHeight() + pos.getY());
	        GL11.glTexCoord2f(0, 1);
	        GL11.glVertex2f(pos.getX(), tex.getTextureHeight() + pos.getY());
        GL11.glEnd();
	}
}
