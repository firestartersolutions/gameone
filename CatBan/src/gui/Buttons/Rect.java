package gui.Buttons;

import org.lwjgl.util.Point;
import org.newdawn.slick.opengl.Texture;

import gui.TexturesPack.*;

public class Rect implements Shape {
    protected int width;
    protected int height;
    public Point location;
    private Texture mt;
    
    public Rect() { };
    
    public Rect(Point location) {
    	this.location = location;
    }
    
    public int getWidth() {
        return this.width;
    }
    
    public int getHeight() {
        return this.height;
    }
    
    // ������ ������ ��������
    public void setSize(int w, int h) {
        if (w > 0 && h > 0) {
        	this.width = w;
        	this.height = h;
        }
        else
        	throw new IllegalArgumentException("Incorrect width and/or height: w "+ w + ", h " + h);
    }

    @Override
    public boolean isInBounds(Point cursorPos) { // �������� �� ������� ������ ����
        return cursorPos.getX() >= location.getX() &&
               cursorPos.getY() >= location.getY() &&
               cursorPos.getX() <= location.getX() + width &&
               cursorPos.getY() <= location.getY() + height;
    }

	@Override
	public void setTexture(String texName) {
		mt = TexturesLoader.loadTexture(texName);
	}

	@Override
	public void printTexture() { // ��������� ��������
		Point pos = this.getLocation();
		TexturesLoader.textureRenderPoint(mt, pos);
	}
	
    @Override
    public void setLocation(Point location) {
        this.location = location;
    }
    
    @Override
    public void setLocation(int x, int y) {
        this.location.setX(x);
        this.location.setY(y);
    }
    
    @Override
    public Point getLocation() {
        return this.location;
    }
}
