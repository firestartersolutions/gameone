package gui.Buttons;
import org.lwjgl.util.Point;

public interface Shape {
    public void setLocation(Point location);
    public void setLocation(int x, int y);
    public Point getLocation();
    
    public void setTexture(String texName);
    public void printTexture();

    public boolean isInBounds(Point cursorPos);
}
