package gui.Buttons;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Point;

import gui.Buttons.MenuItems.YesNoExit;

public class ClickEvent {
    protected Point cursorPos;
    
    public ClickEvent(Point cursorPos) {
        this.cursorPos = cursorPos;
    }

    public Point getCursorPos() {
        return this.cursorPos;
    }
    
    //----------�����(��/���)
    public static void doExit() {
    	if(Display.isCloseRequested()) {	// ���� ����� �������
        	YesNoExit yesno = new YesNoExit();
        	
        	yesno.initTextures(); // ������������� �������
        	yesno.initButtons(); // ������������� ������
			
        	while(ButtonPanel.pr) {	
				GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
				
				yesno.handleInput(); // �������� �� ������� ������
				yesno.render();		// ���� ������ ������ ��
				
		        Display.update(); // ���������� ������ 
		    }
        	ButtonPanel.pr = true;
        }
    }
}
