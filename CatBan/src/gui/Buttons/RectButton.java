package gui.Buttons;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.Point;

public class RectButton extends Rect implements Button {
	protected ActionListener onClickListener;
    
    public RectButton(Point location) {
        super(location);
    }
    
	@Override
    public void setOnClickListener(ActionListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public void dispatch(ClickEvent event) {
    	if(this.isInBounds(event.getCursorPos()) && Mouse.isButtonDown(0)) {
    		while (Mouse.isButtonDown(0)) {
    			Display.update();
    			
    			try {
    				Thread.sleep(5);
    			} catch (InterruptedException e) {
    				e.printStackTrace();
    			}
    		}
    		onClickListener.actionPerformed();
    	}
    }

    public void performClick() {
        onClickListener.actionPerformed();
    }
}
