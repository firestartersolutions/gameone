package gui.Buttons;

import java.util.ArrayList;
import java.util.List;
import org.lwjgl.util.Point;

public class ButtonPanel extends Rect {
	
    public static boolean pr = true;
	List<RectButton> bList;
	List<RectButton> tempList;
	
	Point coord = new Point(0,0);
	
	// ������ ������
	public ButtonPanel(Point location) {
		super(location);
		
		bList = new ArrayList<RectButton>(5);
		tempList = new ArrayList<RectButton>(2);
	}
	
	public ButtonPanel() { }

	public List<RectButton> getbList() {
		return bList;
	}
	
	// ��������� ������
	public void renderPanel() {
		for(RectButton b: bList)
			b.printTexture();
	}
	
	// ���������� ������
	public void addButton(RectButton button) {
		bList.add(button);
	}
}
