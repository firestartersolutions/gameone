package gui.Buttons;

public interface Button {
	public void setOnClickListener(ActionListener onClickListener);
    public void dispatch(ClickEvent event);
    public void performClick();
}
