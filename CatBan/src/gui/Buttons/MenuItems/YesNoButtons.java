package gui.Buttons.MenuItems;

import java.util.ArrayList;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Point;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import gui.Buttons.ActionListener;
import gui.Buttons.ButtonPanel;
import gui.Buttons.ClickEvent;
import gui.Buttons.RectButton;
import gui.TexturesPack.*;

public class YesNoButtons implements Menu {
	protected Texture bg;
	protected Texture sp;
	protected ButtonPanel bp;
	protected ArrayList<ActionListener> actList;
	
	@Override
	public void initTextures() {
		this.initTextures();
	}

	@Override
	public void initButtons() {
		bp = new ButtonPanel(new Point(Display.getWidth()*33/100, Display.getHeight()*53/100));		// ����� ������� ���� ������
		bp.setSize(Display.getWidth()*34/100, Display.getHeight()*17/100); // ������ ������� ������� ������
		
		actList = new ArrayList<ActionListener>();
		
		this.setActions();		//�������� ������ ��������
		
		//-----------������ ��
		RectButton button1 = new RectButton(new Point(bp.location));
		bp.addButton(button1);
		button1.setLocation(bp.location.getX(), bp.location.getY()); // ������������ ������
		button1.setSize(bp.getWidth()*45/100, bp.getHeight()); // ������ ������
		button1.setTexture("res/pic/Yes.png");				//��������� ��������
		button1.setOnClickListener(actList.get(0));				//���������� �������� �� �������
		
		//-----------������ ���
		RectButton button2 = new RectButton(new Point(bp.location));
		bp.addButton(button2);
		button2.setLocation(bp.location.getX() + bp.getWidth()*50/100, bp.location.getY());
		button2.setSize(bp.getWidth()*45/100, bp.getHeight());
		button2.setTexture("res/pic/No.png");
		button2.setOnClickListener(actList.get(1));
	}

	@Override
	public void handleInput() { //�������� �� ������� ������
		int curX = Mouse.getX(), curY = Display.getHeight() - Mouse.getY();
    	Point pos = new Point(curX, curY);
    	
    	for(RectButton b : bp.getbList())	//��� ������ ������ � ������
     		b.dispatch(new ClickEvent(pos));	//������ �� ������ � ����������� �����������

    	ClickEvent.doExit(); 
	}

	@Override
	public void render() { // ��������� �������
		GL11.glClear( GL11.GL_COLOR_BUFFER_BIT);
		Color.white.bind();
        
        TexturesLoader.textureRenderPoint(bg, new Point(0,0));	//��������� ����
        TexturesLoader.textureRenderPoint(sp, new Point(0,0));	//��������� ��������
        
        bp.renderPanel();	//��������� ������� ������
	}

	@Override
	public void setActions() {
		this.setActions();
	}
}
