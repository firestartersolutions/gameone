package gui.Buttons.MenuItems;

import org.lwjgl.opengl.Display;

import gui.Buttons.ActionListener;
import gui.TexturesPack.TexturesLoader;

public class YesNoExit extends YesNoButtons {
	
	@Override
	public void initTextures() {
		this.bg = TexturesLoader.loadTexture("res/pic/Exit.png");
		this.sp = TexturesLoader.loadTexture("res/pic/SettingsBG.png");
	}
	
	@Override
	public void setActions()
	{
		//--------������ ��
		actList.add(new ActionListener (){
			public void actionPerformed() {
				Display.destroy();
				System.exit(0);
			}
			});
		
		//-------������ ���
		actList.add(new ActionListener() {
			public void actionPerformed() {
				bp.pr = false;
			}
		});
	}
}
