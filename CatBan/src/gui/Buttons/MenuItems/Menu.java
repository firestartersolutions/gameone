package gui.Buttons.MenuItems;

public interface Menu {
	public void initTextures();
	public void initButtons();
	public void handleInput();
	public void render();
	public void setActions();
}
