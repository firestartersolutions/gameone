package gui.Buttons.MenuItems;

import app.Game;
import gui.Buttons.ActionListener;
import gui.TexturesPack.TexturesLoader;

public class YesNoExitPause extends YesNoButtons {
	@Override
	public void initTextures() {
		this.bg = TexturesLoader.loadTexture("res/pic/ExitToMain.png");
		this.sp = TexturesLoader.loadTexture("res/pic/SettingsBG.png");
	}
	
	@Override
	public void setActions() {
		//--------������ �� (����� � ������� ����)
		actList.add(new ActionListener (){
			public void actionPerformed() {
				Game.setIsRunning(false);
			}
		});
		
		//--------������ ��� (������� � �����/�� �������)
		actList.add(new ActionListener() {
			public void actionPerformed() {
				bp.pr = false;
			}
		});
	}
}
