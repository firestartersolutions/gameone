package gui.Buttons.MenuItems;

import java.io.IOException;
import java.util.ArrayList;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Point;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import app.Game;
import gui.Buttons.ActionListener;
import gui.Buttons.ButtonPanel;
import gui.Buttons.ClickEvent;
import gui.Buttons.Rect;
import gui.Buttons.RectButton;
import gui.TexturesPack.Textures;
import gui.TexturesPack.TexturesLoader;
import tools.Serializer;

public class Settings implements Menu {
	Texture settings;
	Texture spiral;
	Texture skin;
	Texture ball;
	Textures tex;
	protected ButtonPanel bp;
	protected ArrayList<ActionListener> actList;
	
	Rect choiseCat;
	private Rect choiseBall;
	
	public void serializationTextures(String fileName) {
		Serializer serializer = new Serializer();
		try {
			serializer.serializeTextures(Game.getTextures(), fileName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void initTextures() { // ������������� ������ 
		settings = TexturesLoader.loadTexture("res/pic/Settings.png");
		spiral = TexturesLoader.loadTexture("res/pic/SettingsBG.png");
		skin = TexturesLoader.loadTexture("res/pic/cat1.png");
		ball = TexturesLoader.loadTexture("res/pic/k1.png");
		serializationTextures("PreviousTextureSettings");
		tex = Game.getTextures();
	}

	@Override
	public void initButtons() { // ������������� ������
		bp = new ButtonPanel(new Point(Display.getWidth()*30/100, Display.getHeight()*31/100)); // ����� ������� ���� ������
		bp.setSize(Display.getWidth()*30/100, Display.getHeight()*60/100);
		
		actList = new ArrayList<ActionListener>();
		
		this.setActions(); // �������� ������ ��������
		
		choiseCat = new Rect(new Point(bp.location));
		
		switch(tex.getPathPlayerTexture()) { //����� ����
			case "res/pic/cat_1.png":  choiseCat.setLocation(bp.location.getX() + bp.getWidth()*6/100, 
					 					   				  bp.location.getY() + bp.getHeight()*30/100);
					 break;
			case "res/pic/cat_2.png":  choiseCat.setLocation(bp.location.getX() + bp.getWidth()*56/100, 
										   bp.location.getY() + bp.getHeight()*30/100);
			 		 break;
			case "res/pic/cat_3.png":  choiseCat.setLocation(bp.location.getX() + bp.getWidth()*106/100, 
										   bp.location.getY() + bp.getHeight()*30/100);
					 break;
			default: choiseCat.setLocation(bp.location.getX() + bp.getWidth()*6/100, 
						  				   bp.location.getY() + bp.getHeight()*30/100);
					 break;
		}
		
		choiseCat.setTexture("res/pic/chosen.png");
		
		choiseBall = new Rect(new Point(bp.location));

		switch(tex.getPathBallTexture()) { //����� ������
			case "res/pic/������_1.png":  choiseBall.setLocation(bp.location.getX() + bp.getWidth()*6/100, 
											bp.location.getY() + bp.getHeight()*77/100);
					 break;
			case "res/pic/������_2.png":  choiseBall.setLocation(bp.location.getX() + bp.getWidth()*56/100, 
											bp.location.getY() + bp.getHeight()*77/100);
			 		 break;
			case "res/pic/������_3.png":  choiseBall.setLocation(bp.location.getX() + bp.getWidth()*106/100, 
											bp.location.getY() + bp.getHeight()*77/100);
					 break;
			default: choiseBall.setLocation(bp.location.getX() + bp.getWidth()*6/100, 
											bp.location.getY() + bp.getHeight()*77/100);
					 break;
		}
		
		choiseBall.setTexture("res/pic/chosen.png");
		
		//-----------���1
		RectButton cat1 = new RectButton(new Point(bp.location));
		bp.addButton(cat1);
		cat1.setLocation(bp.location.getX(), bp.location.getY()); // ������������ ��������
		cat1.setSize(bp.getWidth()*45/100, bp.getHeight()*30/100); // ��������� ������� ��������
		cat1.setTexture("res/pic/cat1.png");				// ��������� ��������
		cat1.setOnClickListener(actList.get(0));				// ���������� �������� �� �������
		
		//-----------���2
		RectButton cat2 = new RectButton(new Point(bp.location));
		bp.addButton(cat2);
		cat2.setLocation(bp.location.getX() + bp.getWidth()*50/100, bp.location.getY());
		cat2.setSize(bp.getWidth()*45/100, bp.getHeight()*30/100);
		cat2.setTexture("res/pic/cat2.png");
		cat2.setOnClickListener(actList.get(1));
		
		//-----------���3
		RectButton cat3 = new RectButton(new Point(bp.location));
		bp.addButton(cat3);
		cat3.setLocation(bp.location.getX() + bp.getWidth(), bp.location.getY());
		cat3.setSize(bp.getWidth()*45/100, bp.getHeight()*30/100);
		cat3.setTexture("res/pic/cat3.png");
		cat3.setOnClickListener(actList.get(2));
		
		//-----------������1
		RectButton ball1 = new RectButton(new Point(bp.location));
		bp.addButton(ball1);
		ball1.setLocation(bp.location.getX(), bp.location.getY() + bp.getHeight()*48/100);
		ball1.setSize(bp.getWidth()*45/100, bp.getHeight()*30/100);
		ball1.setTexture("res/pic/k1.png");		
		ball1.setOnClickListener(actList.get(3));			
		
		//-----------������2
		RectButton ball2 = new RectButton(new Point(bp.location));
		bp.addButton(ball2);
		ball2.setLocation(bp.location.getX() + bp.getWidth()*50/100, bp.location.getY() + bp.getHeight()*48/100);
		ball2.setSize(bp.getWidth()*45/100, bp.getHeight()*30/100);
		ball2.setTexture("res/pic/k2.png");
		ball2.setOnClickListener(actList.get(4));
		
		//-----------������3
		RectButton ball3 = new RectButton(new Point(bp.location));
		bp.addButton(ball3);
		ball3.setLocation(bp.location.getX() + bp.getWidth(), bp.location.getY() + bp.getHeight()*48/100);
		ball3.setSize(bp.getWidth()*45/100, bp.getHeight()*30/100);
		ball3.setTexture("res/pic/k3.png");
		ball3.setOnClickListener(actList.get(5));
		
		//-----------���������
		RectButton button1 = new RectButton(new Point(bp.location));
		bp.addButton(button1);
		button1.setLocation(bp.location.getX() + bp.getWidth()*20/100, bp.location.getY() + bp.getHeight()*88/100);
		button1.setSize(bp.getWidth()*45/100, bp.getHeight()*12/100);
		button1.setTexture("res/pic/Save.png");
		button1.setOnClickListener(actList.get(6));
		
		//-----------������
		RectButton button2 = new RectButton(new Point(bp.location));
		bp.addButton(button2);
		button2.setLocation(bp.location.getX() + bp.getWidth()*87/100, bp.location.getY() + bp.getHeight()*88/100);
		button2.setSize(bp.getWidth()*45/100, bp.getHeight()*12/100);
		button2.setTexture("res/pic/Cancel.png");
		button2.setOnClickListener(actList.get(7));
	}

	@Override
	public void handleInput() {
		int curX = Mouse.getX(), curY = Display.getHeight() - Mouse.getY();
    	Point pos = new Point(curX, curY);
    	
    	for(RectButton b : bp.getbList())	//��� ������ ������ � ������
    		b.dispatch(new ClickEvent(pos));	//������ �� ������ � ����������� �����������

    	ClickEvent.doExit();
	}

	@Override
	public void render() {
		GL11.glClear( GL11.GL_COLOR_BUFFER_BIT);
		Color.white.bind();
        
        TexturesLoader.textureRenderPoint(settings, new Point(0,0)); // ��������� ����
        TexturesLoader.textureRenderPoint(spiral, new Point(0,0)); // ��������� ��������
        
        bp.renderPanel();	//��������� ������� ������
        choiseCat.printTexture();
        choiseBall.printTexture();
	}
	
	@Override
	public void setActions() {
		//------����� - ���1
		actList.add(new ActionListener () {
			public void actionPerformed() {
				skin = TexturesLoader.loadTexture("res/pic/cat1.png"); //����������� �������� ����
				choiseCat.setLocation(bp.location.getX() + bp.getWidth()*6/100, bp.location.getY() + bp.getHeight()*30/100); //������������ �������� "�������"
				Game.setPlayerTexture("res/pic/cat_1.png"); //���������� ���������� ���� � ����
			}
		});
		
		//------����� - ���2
		actList.add(new ActionListener () {
			public void actionPerformed() {
				skin = TexturesLoader.loadTexture("res/pic/cat2.png");
				choiseCat.setLocation(bp.location.getX() + bp.getWidth()*56/100, bp.location.getY() + bp.getHeight()*30/100);
				Game.setPlayerTexture("res/pic/cat_2.png");
			}
		});
				
		//------����� - ���3
		actList.add(new ActionListener () {
			public void actionPerformed() {
				skin = TexturesLoader.loadTexture("res/pic/cat3.png");
				choiseCat.setLocation(bp.location.getX() + bp.getWidth()*106/100, bp.location.getY() + bp.getHeight()*30/100);
				Game.setPlayerTexture("res/pic/cat_3.png");
			}
		});
		
		//------������ - ������1
		actList.add(new ActionListener () {
			public void actionPerformed() {
				ball = TexturesLoader.loadTexture("res/pic/k1.png");
				choiseBall.setLocation(bp.location.getX() + bp.getWidth()*6/100, bp.location.getY() + bp.getHeight()*77/100);
				Game.setBallTexture("res/pic/������_1.png");
				Game.setAlternateBoxTexture("res/pic/����_1.png");
			}
		});
		
		//------������ - ������2
		actList.add(new ActionListener () {
			public void actionPerformed() {
				ball = TexturesLoader.loadTexture("res/pic/k2.png");
				choiseBall.setLocation(bp.location.getX() + bp.getWidth()*56/100, bp.location.getY() + bp.getHeight()*77/100);
				Game.setBallTexture("res/pic/������_2.png");
				Game.setAlternateBoxTexture("res/pic/����_2.png");
			}
		});
				
		//------������ - ������3
		actList.add(new ActionListener () {
			public void actionPerformed() {
				ball = TexturesLoader.loadTexture("res/pic/k3.png");
				choiseBall.setLocation(bp.location.getX() + bp.getWidth()*106/100, bp.location.getY() + bp.getHeight()*77/100);
				Game.setBallTexture("res/pic/������_3.png");
				Game.setAlternateBoxTexture("res/pic/����_3.png");
			}
		});
		
		//------���������
		actList.add(new ActionListener () {
			public void actionPerformed() {
				//������������ ����� TextureSettings
				//new File("TextureSettings.out").delete();
				serializationTextures("TextureSettings");
				
				bp.pr = false;
			}
		});
		
		//------������
		actList.add(new ActionListener () {
			public void actionPerformed() {
				Game.texturesDeserialization("PreviousTextureSettings");
				bp.pr = false;
	 		}
		});
	}
}
