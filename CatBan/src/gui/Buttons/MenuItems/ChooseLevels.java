package gui.Buttons.MenuItems;

import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Point;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import app.Account;
import app.Game;
import gui.CompleteLevel;
import gui.Buttons.ActionListener;
import gui.Buttons.ButtonPanel;
import gui.Buttons.ClickEvent;
import gui.Buttons.RectButton;
import gui.TexturesPack.TexturesLoader;

public class ChooseLevels implements Menu {
	private Texture resG;
	private ButtonPanel bp;
	private ArrayList<ActionListener> actList;
	private Texture[] levelStars = new Texture[6];
	
	@Override
	public void initTextures() {
		resG = TexturesLoader.loadTexture("res/pic/ChooseLevels.png");
		for(int i = 0; i < levelStars.length; i++) {
			switch(Account.getInstance().getStars(i)) 
			{
				case 1:  levelStars[i] = TexturesLoader.loadTexture("res/pic/0_1.png");
						 break;
				case 2:  levelStars[i] = TexturesLoader.loadTexture("res/pic/0_2.png");
				 		 break;
				case 3:  levelStars[i] = TexturesLoader.loadTexture("res/pic/0_3.png");
						 break;
				default: levelStars[i] = TexturesLoader.loadTexture("res/pic/0_0.png"); // �������� �� ���������
						 break;
			}
		}
	}

	@Override
	public void handleInput() {
		int curX = Mouse.getX(), curY = Display.getHeight() - Mouse.getY();
    	Point pos = new Point(curX, curY);
    	
    	for(RectButton b : bp.getbList())	//��� ������ ������ � ������
    		b.dispatch(new ClickEvent(pos));	//������ �� ������ � ����������� �����������

		ClickEvent.doExit();
	}

	@Override
	public void render() {
		TexturesLoader.textureRenderPoint(resG, new Point(0,0));
		//����������� ��������� � "����� ������"
		TexturesLoader.textureRenderPoint(levelStars[0], new Point(Display.getWidth()*12/100, Display.getHeight()*49/100));
		TexturesLoader.textureRenderPoint(levelStars[1], new Point(Display.getWidth()*43/100, Display.getHeight()*49/100));
		TexturesLoader.textureRenderPoint(levelStars[2], new Point(Display.getWidth()*74/100, Display.getHeight()*49/100));
		TexturesLoader.textureRenderPoint(levelStars[3], new Point(Display.getWidth()*12/100, Display.getHeight()*82/100));
		TexturesLoader.textureRenderPoint(levelStars[4], new Point(Display.getWidth()*43/100, Display.getHeight()*82/100));
		TexturesLoader.textureRenderPoint(levelStars[5], new Point(Display.getWidth()*74/100, Display.getHeight()*82/100));
		
		bp.renderPanel();
	}

	@Override
	public void initButtons() {		
		bp = new ButtonPanel(new Point(Display.getWidth()*8/100, Display.getHeight()*24/100)); // ����� ������� ���� ������
		bp.setSize(Display.getWidth()*75/100, Display.getHeight()*80/100);
		
		actList = new ArrayList<ActionListener>(); // ������ �������
		
		setActions(); // �������� ������ ��������
		
		// 1 �������
		RectButton button1 = new RectButton(new Point(bp.location));
		bp.addButton(button1);
		button1.setLocation(bp.location.getX(), bp.location.getY()); // ����������� ������
		button1.setSize(bp.getWidth()*33/100, bp.getHeight()*48/100); // ��������� ������� ������
		button1.setTexture("res/pic/level1.png");	// ��������� ��������
		button1.setOnClickListener(actList.get(0));		// ���������� �������� �� �������
		
		// 2 �������
		RectButton button2 = new RectButton(new Point(bp.location));
		bp.addButton(button2);
		button2.setLocation(bp.location.getX() + bp.getWidth()*41/100, bp.location.getY());
		button2.setSize(bp.getWidth()*33/100, bp.getHeight()*48/100);
		button2.setTexture("res/pic/level2.png");
		button2.setOnClickListener(actList.get(1));
		
		// 3 �������
		RectButton button3 = new RectButton(new Point(bp.location));
		bp.addButton(button3);
		button3.setLocation(bp.location.getX() + bp.getWidth()*82/100, bp.location.getY());
		button3.setSize(bp.getWidth()*33/100, bp.getHeight()*48/100);
		button3.setTexture("res/pic/level3.png");
		button3.setOnClickListener(actList.get(2));
		
		// 4 �������
		RectButton button4 = new RectButton(new Point(bp.location));
		bp.addButton(button4);
		button4.setLocation(bp.location.getX(), bp.location.getY() + bp.getHeight()*41/100);
		button4.setSize(bp.getWidth()*33/100, bp.getHeight()*48/100);
		button4.setTexture("res/pic/level4.png");
		button4.setOnClickListener(actList.get(3));
		
		// 5 �������
		RectButton button5 = new RectButton(new Point(bp.location));
		bp.addButton(button5);
		button5.setLocation(bp.location.getX() + bp.getWidth()*41/100, bp.location.getY() + bp.getHeight()*41/100);
		button5.setSize(bp.getWidth()*33/100, bp.getHeight()*48/100);
		button5.setTexture("res/pic/level5.png");
		button5.setOnClickListener(actList.get(4));
		
		// 6 �������
		RectButton button6 = new RectButton(new Point(bp.location));
		bp.addButton(button6);
		button6.setLocation(bp.location.getX() + bp.getWidth()*82/100, bp.location.getY() + bp.getHeight()*41/100);
		button6.setSize(bp.getWidth()*33/100, bp.getHeight()*48/100);
		button6.setTexture("res/pic/level6.png");
		button6.setOnClickListener(actList.get(5));
	}
	
	@Override
	public void setActions() {
		// 1 �������
		actList.add(new ActionListener () {
			public void actionPerformed() {
				actLevel(0);
			}
		});
		
		// 2 �������
		actList.add(new ActionListener () {
			public void actionPerformed() {
				actLevel(1);
			}
		});
		

		// 3 �������
		actList.add(new ActionListener () {
			public void actionPerformed() {
				actLevel(2);
			}
		});

		// 4 �������
		actList.add(new ActionListener () {
			public void actionPerformed() {
				actLevel(3);
			}
		});

		// 5 �������
		actList.add(new ActionListener () {
			public void actionPerformed() {
				actLevel(4);
			}
		});

		// 6 �������
		actList.add(new ActionListener () {
			public void actionPerformed() {
				actLevel(5);
			}
		});
	}
	
	private void actLevel(int i) {
		
		Game.init();
		Game.levelInit(i); // �������������� ����
    	
    	while(Game.getIsRunning())
    	{
    		bp.pr = true;
    		
    		Game.pollInput(); // ��������� �������
    		Game.render();	  // ����������� ������
    		
    		if(Game.checkCurrentLevelPassing()) {		 // �������� ����������� �������� ������
    			
    			// �������� ������� ��������� � ���� �� ����� ���������, �� ������� ��������
    			if((Account.getInstance().getHighscore(i) == 0) || 
    					(Account.getInstance().getHighscore(i) > Game.getLevelList().get(i).getStepCount())) {
    				Account.getInstance().setHighScore(i, Game.getLevelList().get(i).getStepCount());
    				Account.getInstance().highscoresSerialization(Account.getInstance());
    			}
    			
    			CompleteLevel completeLevel = new CompleteLevel
    					(Game.GetResultLevelPassing(Game.getLevelList().get(i).getStepCount()));

    			Game.getLevelList().get(i).setIsCompleted(false);
				completeLevel.initTextures();
				completeLevel.initButtons();
				
    			while(!Game.getIsRunning()) { // ������� ���� ������������

    				Game.render();
    				
    				completeLevel.render();
    				completeLevel.handleInput();
    				
    				Display.update();
	        		
    				try {
    					Thread.sleep(50);
    				} catch (InterruptedException e) {
    					e.printStackTrace();
    				}
    			}
    		}

    		try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
    		
    		Display.update();
    		
    		if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
    		{
	        	//���� ������ Esc; ����������� �����
	        	Pause pause = new Pause();
	        	pause.initTextures();
	        	pause.initButtons();
	        	
	        	while(Game.getIsRunning() && bp.pr)	//���� �� ������ ������ �� � ������ � ����� ��� esc
	        	{	
	        		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
	        		Color.white.bind();
	        		
	        		Game.render(); 
        			
	        		pause.handleInput();
	    	        pause.render();	
	    	        
	    	        Display.update();
	        	}
	        	bp.pr = false;
    		}

    		ClickEvent.doExit();
    	}
	}
}
