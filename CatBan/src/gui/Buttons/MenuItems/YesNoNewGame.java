package gui.Buttons.MenuItems;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;

import app.Account;
import gui.Buttons.ActionListener;
import gui.TexturesPack.TexturesLoader;

public class YesNoNewGame extends YesNoButtons {
	
	@Override
	public void initTextures() { // ������������� �������
		this.bg = TexturesLoader.loadTexture("res/pic/NewGame1.png");
		this.sp = TexturesLoader.loadTexture("res/pic/SettingsBG.png");
	}
	
	@Override
	public void setActions() {
		//---------������ ��
		actList.add(new ActionListener () {
			public void actionPerformed() {
				Account.getInstance().init(); // ������� ������ ����������, ����� ����������� �� � ����
	        	Account.getInstance().highscoresSerialization(Account.getInstance());
				
				ChooseLevels chlev = new ChooseLevels();
				
				chlev.initTextures(); // ������������� ������� �������
				chlev.initButtons(); // ������������� ������
				
				while(bp.pr && !Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) { // ���� �� ����� Esc
					GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
					Color.white.bind();
					
					chlev.handleInput(); // �������� �� ������� ������
					chlev.render(); // ��������� �������
	  				
					Display.update(); // ���������� ������
				}
				if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
				bp.pr = false;
			}
		});
		
		//---------������ ���
		actList.add(new ActionListener () {
			public void actionPerformed() {
				bp.pr = false;
			}
		});
	}
}
