package gui.Buttons.MenuItems;

import gui.Buttons.ActionListener;
import gui.Buttons.ButtonPanel;
import gui.Buttons.ClickEvent;
import gui.Buttons.RectButton;
import gui.TexturesPack.*;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Point;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import app.Account;
 
public class MainMenu implements Menu {
    private Texture bgTexture;
    private Texture cat;
    private ButtonPanel bp;
	public List<ActionListener> actList;
    
    @Override
    public void initTextures() {
         bgTexture = TexturesLoader.loadTexture("res/pic/background.png"); 
         cat = TexturesLoader.loadTexture("res/pic/catmain.png");
    }
    
    //----------�������� ������
	@Override
	public void initButtons() {
		bp = new ButtonPanel(new Point(Display.getWidth()*10/100, Display.getHeight()*15/100));	// ����� ������� ���� ������
		bp.setSize(Display.getWidth()*34/100, Display.getHeight()*71/100);
		
		actList = new ArrayList<ActionListener>(); // ������ ������
		
		setActions();	// �������� ������ ��������
		
		//-----------����������
		RectButton button1 = new RectButton(new Point(bp.location));
		bp.addButton(button1);
		button1.setLocation(bp.location.getX(), bp.location.getY()); // ������������ ������
		button1.setSize(bp.getWidth(), bp.getHeight()*17/100);		// �������� ������� ������
		button1.setTexture("res/pic/����������.png");				// ��������� ��������
		button1.setOnClickListener(actList.get(0));				// ���������� �������� �� �������
		
		//-----------����� ����
		RectButton button2 = new RectButton(new Point(bp.location));
		bp.addButton(button2);
		button2.setLocation(bp.location.getX(), bp.location.getY() + bp.getHeight()*20/100);
		button2.setSize(bp.getWidth(), bp.getHeight()*17/100);
		button2.setTexture("res/pic/�����-����.png");
		button2.setOnClickListener(actList.get(1));
		
		//-----------���������
		RectButton button3 = new RectButton(new Point(bp.location));
		bp.addButton(button3);
		button3.setLocation(bp.location.getX(), bp.location.getY() + bp.getHeight()*40/100);
		button3.setSize(bp.getWidth(), bp.getHeight()*17/100);
		button3.setTexture("res/pic/���������.png");	
		button3.setOnClickListener(actList.get(2));	
		
		//-----------������
		RectButton button4 = new RectButton(new Point());
		bp.addButton(button4);
		button4.setLocation(bp.location.getX(), bp.location.getY() + bp.getHeight()*60/100);
		button4.setSize(bp.getWidth(), bp.getHeight()*17/100);
		button4.setTexture("res/pic/������.png");	
		button4.setOnClickListener(actList.get(3));	
		
		//-----------�����
		RectButton button5 = new RectButton(new Point());
		bp.addButton(button5);
		button5.setLocation(bp.location.getX(), bp.location.getY() + bp.getHeight()*80/100);
		button5.setSize(bp.getWidth(), bp.getHeight()*17/100);
		button5.setTexture("res/pic/�����.png");	
		button5.setOnClickListener(actList.get(4));	
	}
    
    @Override
    public void handleInput() {
    	int curX = Mouse.getX(), curY = Display.getHeight() - Mouse.getY();
    	Point pos = new Point(curX, curY);
    	
    	for(RectButton b : bp.getbList())	// ��� ������ ������ � ������
    		b.dispatch(new ClickEvent(pos));	// ������ �� ������ � ����������� �����������

        ClickEvent.doExit();
    }
    
    @Override
    public void render() {
    	GL11.glClear( GL11.GL_COLOR_BUFFER_BIT);
    	Color.white.bind();
    	
        TexturesLoader.textureRenderPoint(bgTexture, new Point(0,0));	// ��������� ����
        TexturesLoader.textureRenderPoint(cat, new Point(Display.getWidth()*53/100, Display.getHeight()*17/100));	// ��������� ���� �� ��. ������
        
        bp.renderPanel();	// ��������� ������� ������
    }
    
	public void setActions() {		
		//-------------����������
		actList.add(new ActionListener() {
	        public void actionPerformed() {	        
	        	Account.getInstance().highscoresDeserialization();
	        	
				ChooseLevels chlev = new ChooseLevels();
				
				chlev.initTextures(); // ������������� �������
				chlev.initButtons(); // ������������� ������
				
				while(bp.pr && !Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) { // ���� �� ����� Esc
					GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
					Color.white.bind();
					
					chlev.handleInput(); // �������� �� ������� ������
					chlev.render(); // ��������� �������
	  				
					Display.update(); // ���������� ������
				}
				bp.pr = true;
	        };
		});

		//--------------����� ����
		actList.add(1, new ActionListener() {
			public void actionPerformed() {
				
		        YesNoNewGame yesno = new YesNoNewGame();
		        	
		        yesno.initTextures();
		        yesno.initButtons();
		        	
				while(bp.pr) {	// ���� �� ������ ������ ���
					GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
					Color.white.bind();
					
					yesno.render();
					yesno.handleInput();
					
				    Display.update();
				}
				bp.pr = true;
			}
		});
		
		//------------------���������
		actList.add(2, new ActionListener() {
	          public void actionPerformed() {
	        	Settings settings = new Settings();
	        	
	        	settings.initTextures();
	        	settings.initButtons();
	  			
	  			while(bp.pr) { // ���� �� ����� ������
	  				GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
	  				Color.white.bind();
	  				
	  				settings.handleInput();
	  				settings.render();

	  				ClickEvent.doExit();
	  				
	  		        Display.update();
	  			}
	  			bp.pr= true;
	          }
	    });
	      
	      //-----------------������
		 actList.add(3, new ActionListener() {
	          public void actionPerformed() {
	           Texture help;
	  			help = TexturesLoader.loadTexture("res/pic/Help.png");
	  			
	  			while(!Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) { // ���� �� ����� Esc
	  				GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
	  				Color.white.bind();
	  				
	  				TexturesLoader.textureRenderPoint(help, new Point(0,0));
	  				
	  				ClickEvent.doExit();
	  				
	  		        Display.update();
	  			}
	          }
	      });
		 
		 //-----------------------�����
	     actList.add(4, new ActionListener() {
	    	  	public void actionPerformed() {
	    	  		YesNoExit yesno = new YesNoExit();
		        	
		        	yesno.initTextures();
		        	yesno.initButtons();
	  			
		        	while(bp.pr) {		// ���� �� ������ ������ ���
						GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
						Color.white.bind();
						
						yesno.handleInput();
						yesno.render();		
						
				        Display.update();
				    }
		        	bp.pr = true;
	    	  	}
	     });
	}
}