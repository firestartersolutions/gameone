package gui;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.Point;
import org.newdawn.slick.Color;
import org.newdawn.slick.opengl.Texture;

import gui.Buttons.ActionListener;
import gui.Buttons.ButtonPanel;
import gui.Buttons.ClickEvent;
import gui.Buttons.RectButton;
import gui.Buttons.MenuItems.Menu;
import gui.TexturesPack.TexturesLoader;
import app.Account;
import app.Game;

public class CompleteLevel implements Menu {
	int stars;
	Texture congrats;
	ButtonPanel bp;
	List<ActionListener> actList;
	
	public CompleteLevel() {}
	
	@Override
	public void initTextures() {
		congrats = TexturesLoader.loadTexture("res/pic/LevelComplete.png"); 
	}
	
	public CompleteLevel(int stars) {
		this.stars = stars; 
	}
	
    public void handleInput() {
    	int curX = Mouse.getX(), curY = Display.getHeight() - Mouse.getY();
    	Point pos = new Point(curX, curY);
    	
    	for(RectButton b : bp.getbList())	//��� ������ ������ � ������
    		b.dispatch(new ClickEvent(pos));	//������ �� ������ � ����������� �����������
    
        ClickEvent.doExit();
    }
	
	public void render() {
		Color.white.bind();
		
		TexturesLoader.textureRenderPoint(congrats, new Point(0,0)); // ��������� ����� ������
	
    	if(this.stars == 3) // ��������� ��������� ��� ����������� ������ �� "�������"
    		TexturesLoader.textureRenderXYCoordinates(Game.getTextures().getPerfectPassingTexture(), 
    			(Display.getWidth() / 2) - (Game.getTextures().getPerfectPassingTexture().getTextureWidth() / 2) + 40, 275); 
    	else if(this.stars == 2) // ��������� ��������� ��� ����������� ������ �� "������"
    		TexturesLoader.textureRenderXYCoordinates(Game.getTextures().getGoodPassingTexture(), 
    			(Display.getWidth() / 2) - (Game.getTextures().getGoodPassingTexture().getTextureWidth() / 2) + 40, 275); 
    	else // ��������� ��������� ��� ����������� ������
    		TexturesLoader.textureRenderXYCoordinates(Game.getTextures().getPassingTexture(), 
    			(Display.getWidth() / 2) - (Game.getTextures().getPassingTexture().getTextureWidth() / 2) + 40, 275); 
    	
    	bp.renderPanel();
	}
	
	public void initButtons() {
		bp = new ButtonPanel(new Point(Display.getWidth()*38/100, Display.getHeight()*60/100));		//����� ������� ���� ������
		bp.setSize(Display.getWidth()*34/100, Display.getHeight()*17/100);
		
		actList = new ArrayList<ActionListener>();
		
		setActions();		//�������� ������ ��������
		
		//-----------�������
		RectButton button1 = new RectButton(new Point(bp.location));
		bp.addButton(button1);
		button1.setLocation(bp.location.getX(), bp.location.getY());
		button1.setSize(bp.getWidth()*45/100, bp.getHeight());
		button1.setTexture("res/pic/Restart.png");				//��������� ��������
		button1.setOnClickListener(actList.get(0));				//���������� �������� �� �������
		
		//------------�����
		RectButton button2 = new RectButton(new Point(bp.location));
		bp.addButton(button2);
		button2.setLocation(bp.location.getX() + bp.getWidth()*45/100, bp.location.getY());
		button2.setSize(bp.getWidth()*45/100, bp.getHeight());
		button2.setTexture("res/pic/NextLevel.png");
		button2.setOnClickListener(actList.get(1));
	}
	
	public void setActions() {
		//------------�������
		actList.add(new ActionListener() {
	        public void actionPerformed() {        	
	        	Game.init();
	        	Game.levelInit(Game.getCurrentLevelIndex());
	        }
		});
		
		//--------------��������� �������
		actList.add(new ActionListener() {
	        public void actionPerformed() {
	        	
	        	if((Account.getInstance().getHighscore(Game.getCurrentLevelIndex()) == 0) || 
    					(Account.getInstance().getHighscore(Game.getCurrentLevelIndex()) > Game.getLevelList().get(Game.getCurrentLevelIndex()).getStepCount())) {
    				Account.getInstance().setHighScore(Game.getCurrentLevelIndex(), Game.getLevelList().get(Game.getCurrentLevelIndex()).getStepCount());
    				Account.getInstance().highscoresSerialization(Account.getInstance());
    			}
	        	
	        	Game.setIsRunning(true);
	        	Game.levelInit(Game.getCurrentLevelIndex()+1);
	        }
		});
	}
}
