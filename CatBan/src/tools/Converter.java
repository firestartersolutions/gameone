package tools;

import org.lwjgl.opengl.Display;

public final class Converter {
	
	public static int xCoordinateToPixels(int xCoordinate, int leftBoard, int entityTextureSize) {
		return leftBoard + xCoordinate * entityTextureSize;
	}
	
	public static int yCoordinateToPixels(int yCoordinate, int upBoard, int entityTextureSize) {
		return Display.getHeight() - (upBoard + (yCoordinate + 1) * entityTextureSize);
	}
	
	public static int coordinateToPixels(int value, int entityTextureSize) {
		return value * entityTextureSize;
	}
	
	public static int levelWidthToLeftBoard(int levelWidth, int entityTextureSize) {
		return (Display.getWidth() - coordinateToPixels(levelWidth, entityTextureSize)) / 2;
	}
	
	public static int levelHeightToUpBoard(int levelHeight, int entityTextureSize) {
		return (Display.getHeight() - coordinateToPixels(levelHeight, entityTextureSize)) / 2;
	}
	
	static public int getClosestPower2(int number) {
        int result = 2;
        while (result < number)
        	result *= 2;
        return result;
    }
	
	static public int getClosestPowerValue(int number, int value) {
        int result = value;
        while (result < number)
        	result *= value;
        return result;
    }
}
