package tools;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import app.Account;
import gui.TexturesPack.Textures;
import logic.Level;

public class Serializer {
	// ������������ �������
	public void serializeLevel(Level level, String levelName) throws IOException {
		FileOutputStream fos = new FileOutputStream(levelName + ".out");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(level);
		oos.flush();
		oos.close();
	}
	
	// �������������� �������
	public Level deserializeLevel(String levelName) throws IOException, ClassNotFoundException {
		FileInputStream fis = new FileInputStream(levelName + ".out");
		ObjectInputStream oin = new ObjectInputStream(fis);
		Level level = (Level) oin.readObject();
		oin.close();
		return level;
	}
	
	// ������������ ��������
	public void serializeTextures(Textures textures, String fileName) throws IOException {
		FileOutputStream fos = new FileOutputStream(fileName + ".out");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(textures);
		oos.flush();
		oos.close();
	}
	
	// �������������� ��������
	public Textures deserializeTextures(String fileName) throws ClassNotFoundException, IOException {
		FileInputStream fis = new FileInputStream(fileName + ".out");
		ObjectInputStream oin = new ObjectInputStream(fis);
		Textures texture = (Textures) oin.readObject();
		oin.close();
		return texture;
	}
	
	// ������������ ������ �����������
	public void serializeHighscores(Account account, String fileName) throws IOException {
		FileOutputStream fos = new FileOutputStream(fileName + ".out");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		try {
			oos.writeObject(account);
        } catch (IOException e) {
            e.printStackTrace();
        }
		oos.flush();
		oos.close();
	}
	
	// �������������� ������ �����������
	public Account deserializeHighscores(String fileName) throws ClassNotFoundException, IOException {
		FileInputStream fis = new FileInputStream(fileName + ".out");
		ObjectInputStream oin = new ObjectInputStream(fis);
		Account account = (Account) oin.readObject();
		oin.close();
		return account;
	}
	
}
