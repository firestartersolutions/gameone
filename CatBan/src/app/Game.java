package app;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;

import java.awt.Font;

import org.newdawn.slick.Color;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.util.ResourceLoader;

import gui.TexturesPack.*;
import logic.Level;
import logic.StateHandler;
import tools.Converter;
import tools.Serializer;

public class Game {	
	private static ArrayList<Level> levelList = new ArrayList<Level>(); // ������ �������
	
	private static Textures textures = new Textures("res/pic/background.png", "res/pic/cat_1.png", 
			"res/pic/������_1.png", "res/pic/����_1.png", "res/pic/wall.png", "res/pic/���.png", 
			"res/pic/�������.png", "res/pic/Congrats.png", "res/pic/1_1.png", "res/pic/1_2.png", 
			"res/pic/1_3.png", "res/pic/SettingsBG.png", "res/pic/Steps.png"); // ������� ��������
	
	private static boolean isRunning; // ������������� ������� ������
	
	private static int currentLevelIndex; // ����� �������� ������
	
	private static int leftBoard; // ���������� � �������� �� ������ ���� ������ �� �������� ������������
	private static int upBoard;	// ���������� � �������� �� �������� ���� ������ �� �������� ������������
	private static int entityTextureSize; // ������ � ������ �������� �������� �� ������� ����
	
	private static TrueTypeFont font;
	private static boolean antiAlias = true;
	
	static {
		//setLeftBoard(160); // ����� �������
		//setUpBoard(64); // ������� �������
		setEntityTextureSize(48); // ������ ��������
	}
	
	public static void levelsDeserialization() {
		levelList.clear();
		Serializer serializer = new Serializer();
		
		try { // �������������� �������
			levelList.add(serializer.deserializeLevel("res/lvl/level1"));
			levelList.add(serializer.deserializeLevel("res/lvl/level2"));
			levelList.add(serializer.deserializeLevel("res/lvl/level3"));
			levelList.add(serializer.deserializeLevel("res/lvl/level4"));
			levelList.add(serializer.deserializeLevel("res/lvl/level5"));
			levelList.add(serializer.deserializeLevel("res/lvl/level6"));	
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// ������������ �������
	public static void levelSerialization(Level level) {
		Serializer serializer = new Serializer();
		
		try {
			serializer.serializeLevel(level, level.getLevelName());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// �������������� �������
	public static void texturesDeserialization(String fileName) {
		Serializer serializer = new Serializer();
		
		try {
			textures = serializer.deserializeTextures(fileName);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		textures.pathToTextures();
	}
	
	// ������ ��� �������� ����
	public static void init() { 
		texturesDeserialization("TextureSettings");
		
		levelsDeserialization();
		
		Keyboard.enableRepeatEvents(true);
		
		try {
            InputStream inputStream = ResourceLoader.getResourceAsStream("res/fnt/myFont.ttf"); // ��������� ������
            
            Font awtFont = Font.createFont(Font.TRUETYPE_FONT, inputStream);
            awtFont = awtFont.deriveFont(32f); // ��������� ������� ������
            
            font = new TrueTypeFont(awtFont, antiAlias);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	// ������������� ��� ������� ������
	public static void levelInit(int i) {	
		setCurrentLevelIndex(i); // ��������� ������ �������
		setLeftBoard(Converter.levelWidthToLeftBoard(levelList.get(currentLevelIndex).getLevelWidth(),
				getEntityTextureSize())); // �������������
		setUpBoard(Converter.levelHeightToUpBoard(levelList.get(currentLevelIndex).getLevelHeight(),
				getEntityTextureSize()));
		
		isRunning = true;
    }
	
	// ����������� ������� � ������� ������ ����������
	public static void pollInput() {
        while (Keyboard.next()) {
        	if (Keyboard.getEventKeyState()) {
        		if (Keyboard.getEventKey() == Keyboard.KEY_W || Keyboard.getEventKey() == Keyboard.KEY_UP) 
        			StateHandler.stepUp(levelList.get(currentLevelIndex));
        		
        		if (Keyboard.getEventKey() == Keyboard.KEY_A || Keyboard.getEventKey() == Keyboard.KEY_LEFT)
        			StateHandler.stepLeft(levelList.get(currentLevelIndex));
        		
        		if (Keyboard.getEventKey() == Keyboard.KEY_S || Keyboard.getEventKey() == Keyboard.KEY_DOWN)
        			StateHandler.stepDown(levelList.get(currentLevelIndex));
        		
        		if (Keyboard.getEventKey() == Keyboard.KEY_D || Keyboard.getEventKey() == Keyboard.KEY_RIGHT)
        			StateHandler.stepRight(levelList.get(currentLevelIndex));
        		
        		if (Keyboard.getEventKey() == Keyboard.KEY_Z)
        			StateHandler.stepUndo(levelList.get(currentLevelIndex));
        		        		
        		StateHandler.checkBoxesPlaced(levelList.get(currentLevelIndex));
        	}
        }
    }
	
	public static void render() {
		GL11.glClear( GL11.GL_COLOR_BUFFER_BIT);
		Color.white.bind();
		
    	TexturesLoader.textureRenderXYCoordinates(getTextures().getSpiralBackgroundTexture(), 0, 0); // ��������� ����
    	
    	for(int i = 0; i < levelList.get(currentLevelIndex).getFloorList().size(); i++) {
    		TexturesLoader.textureRenderXYCoordinates(getTextures().getFloorTexture(),
    			Converter.xCoordinateToPixels(levelList.get(currentLevelIndex).getFloorList().get(i).getXCoordinate(), 
        			getLeftBoard(), getEntityTextureSize()),
    			Converter.yCoordinateToPixels(levelList.get(currentLevelIndex).getFloorList().get(i).getYCoordinate(),
            			getUpBoard(), getEntityTextureSize()));
    	}
    	
    	for(int i = 0; i < levelList.get(currentLevelIndex).getEndZoneList().size(); i++) { // ��������� �����
        	TexturesLoader.textureRenderXYCoordinates(getTextures().getEndZoneTexture(), 
        		Converter.xCoordinateToPixels(levelList.get(currentLevelIndex).getEndZoneList().get(i).getXCoordinate(), 
        			getLeftBoard(), getEntityTextureSize()), 
        		Converter.yCoordinateToPixels(levelList.get(currentLevelIndex).getEndZoneList().get(i).getYCoordinate(),
        			getUpBoard(), getEntityTextureSize()));
    	}
    	
    	TexturesLoader.textureRenderXYCoordinates(getTextures().getPlayerTexture(), // ��������� ������
    		Converter.xCoordinateToPixels(levelList.get(currentLevelIndex).getPlayer().getXCoordinate(),
    			getLeftBoard(), getEntityTextureSize()), 
    		Converter.yCoordinateToPixels(levelList.get(currentLevelIndex).getPlayer().getYCoordinate(),
    			getUpBoard(), getEntityTextureSize()));
    	
    	for(int i = 0; i < levelList.get(currentLevelIndex).getWallList().size(); i++) { // ��������� ����
        	TexturesLoader.textureRenderXYCoordinates(getTextures().getWallTexture(), 
        		Converter.xCoordinateToPixels(levelList.get(currentLevelIndex).getWallList().get(i).getXCoordinate(), 
            			getLeftBoard(), getEntityTextureSize()), 
            	Converter.yCoordinateToPixels(levelList.get(currentLevelIndex).getWallList().get(i).getYCoordinate(),
            			getUpBoard(), getEntityTextureSize()));
    	}
    	
    	for(int i = 0; i < levelList.get(currentLevelIndex).getBoxList().size(); i++) { // ��������� ������
    		if(levelList.get(currentLevelIndex).getBoxList().get(i).getIsPlaced() == true)
    			TexturesLoader.textureRenderXYCoordinates(getTextures().getAlternateBoxTexture(), // ���� ����������� �� ����
    				Converter.xCoordinateToPixels(levelList.get(currentLevelIndex).getBoxList().get(i).getXCoordinate(), 
    	        		getLeftBoard(), getEntityTextureSize()), 
    	        	Converter.yCoordinateToPixels(levelList.get(currentLevelIndex).getBoxList().get(i).getYCoordinate(),
    	        		getUpBoard(), getEntityTextureSize()));
    		else
    			TexturesLoader.textureRenderXYCoordinates(getTextures().getBallTexture(), 
    				Converter.xCoordinateToPixels(levelList.get(currentLevelIndex).getBoxList().get(i).getXCoordinate(), 
    	        			getLeftBoard(), getEntityTextureSize()), 
    	        	Converter.yCoordinateToPixels(levelList.get(currentLevelIndex).getBoxList().get(i).getYCoordinate(),
    	        			getUpBoard(), getEntityTextureSize()));
    	}
    	
    	TexturesLoader.textureRenderXYCoordinates(getTextures().getStepsTexture(), (Display.getWidth() / 2) - 80, 50); // ��������� ����

    	font.drawString((Display.getWidth() / 2), 60, " " + 
    			Integer.toString(levelList.get(currentLevelIndex).getStepCount()), Color.black); // ����� ��-�� ��������� �����
    }
	
	// ���������� ����� ����� ���������� ������ 
	public static int GetResultLevelPassing(int steps) { 
		if(levelList.get(currentLevelIndex).getStepCount() <= levelList.get(currentLevelIndex).getPerfectStepCount())
    		return 3;    	
    	else if(levelList.get(currentLevelIndex).getStepCount() <= levelList.get(currentLevelIndex).getGoodStepCount())
    		return 2;
    	return 1;
	}
	
	// �������� �� ���������� ������
	public static boolean checkCurrentLevelPassing() { 
		StateHandler.checkIsCompleted(levelList.get(currentLevelIndex));
		if(levelList.get(currentLevelIndex).getIsCompleted()) {
			isRunning = false;
			return true;
		}
		return false;
	}
	
	public static ArrayList<Level> getLevelList() {
		return levelList;
	}
	
	public static Textures getTextures() {
		return textures;
	}

	public static void setCurrentLevelIndex(int newIndex) {
		currentLevelIndex = newIndex;
	}
	
	public static int getCurrentLevelIndex() {
		return currentLevelIndex;
	}
	
	public static void setLeftBoard(int value) {
		leftBoard = value;
	}
	
	public static int getLeftBoard() {
		return leftBoard;
	}
	
	public static void setUpBoard(int value) {
		upBoard = value;
	}
	
	public static int getUpBoard() {
		return upBoard;
	}
	
	public static void setEntityTextureSize(int value) {
		entityTextureSize = value;
	}
	
	public static int getEntityTextureSize() {
		return entityTextureSize;
	}
	
	public static void setIsRunning(boolean run) {
		isRunning = run;
	}
	
	public static boolean getIsRunning() {
		return isRunning;
	}
	
	public static void setPlayerTexture(String pathPlayerTexture) {
		textures.setPlayerTexture(pathPlayerTexture);
		textures.setPathPlayerTexture(pathPlayerTexture);
	}
	
	public static void setBallTexture(String pathBallTexture) {
		textures.setBallTexture(pathBallTexture);
		textures.setPathBallTexture(pathBallTexture);
	}
	
	public static void setAlternateBoxTexture(String pathAlternateBoxTexture) {
		textures.setAlternateBoxTexture(pathAlternateBoxTexture);
		textures.setPathAlternateBoxTexture(pathAlternateBoxTexture);
	}
}