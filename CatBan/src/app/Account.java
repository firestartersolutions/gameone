package app;

import java.io.IOException;
import java.io.Serializable;

import app.Game;
import tools.Serializer;

// �������� ������
public class Account implements Serializable { 

	private static final long serialVersionUID = -2883635806100341417L;
	private static Account account; // singleton
	private int[][] highscores; // ������ ���������� �� ����� ��� ������� ������ � ����
	
	private Account() { 
		
	}
	
	public static synchronized  Account getInstance() {
	    if( account==null ) {
	      account = new Account();
	      account.init();
	      account.highscoresDeserialization();
	    }
	    
	    return account;
	  }
	
	public void init() {
		highscores = new int[Game.getLevelList().size()][2]; // ����� �������� | ����� �����
		
		for(int i = 0; i < highscores.length; i++)
			highscores[i][0] = 0;
		for(int i = 0; i < highscores.length; i++)
			highscores[i][1] = 0;
	}
	
	public void setHighScore(int levelID, int stepCount) {
		highscores[levelID][0] = Game.GetResultLevelPassing(stepCount);
		highscores[levelID][1] = stepCount;
	}
	
	public int getStars(int levelID) {
		return highscores[levelID][0];
	}
	
	public int getHighscore(int levelID) {
		return highscores[levelID][1];
	}
	
	public void highscoresDeserialization() { // �������������� �� �����
		Serializer serializer = new Serializer();
		
		try { // �������������� �������
			account = serializer.deserializeHighscores("Highscores");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void highscoresSerialization(Account account) { // ������������ �� ����� 
		Serializer serializer = new Serializer();
		
		try { // ������������ �������
			serializer.serializeHighscores(getInstance(), "Highscores");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
