package logic;

public class EndZone extends FixedEntity {
	private static final long serialVersionUID = 8499148271140456048L;

	public EndZone(int x, int y) {
		this.xCoordinate = x;
		this.yCoordinate = y;
	}
}
