package logic;

public class Player extends MovableEntity {
	private static final long serialVersionUID = -2609500054244860806L;

	public Player(int x, int y) {
		this.xCoordinate = x;
		this.yCoordinate = y;
		this.pastXCoordinate = x;
		this.pastYCoordinate = y;
	}
}
