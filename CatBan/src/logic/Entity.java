package logic;

import java.io.Serializable;

public abstract class Entity implements Serializable {
	private static final long serialVersionUID = -3991358168437236945L;
	
	protected int xCoordinate;
	protected int yCoordinate;
	
	public void setXCoordinate(int x) {
		this.xCoordinate = x;
	}
	
	public int getXCoordinate() {
		return xCoordinate;
	}
	
	public void setYCoordinate(int y) {
		this.yCoordinate = y;
	}
	
	public int getYCoordinate() {
		return yCoordinate;
	}
}
