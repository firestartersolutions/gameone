package logic;

public class Box extends MovableEntity {
	private static final long serialVersionUID = -1381373689424925487L;
	
	private boolean isPlaced = false;
	
	public Box(int x, int y, boolean isPlaced) {
		this.xCoordinate = x;
		this.yCoordinate = y;
		this.pastXCoordinate = x;
		this.pastYCoordinate = y;
		this.isPlaced = isPlaced;
	}
	
	public void setIsPlaced(boolean isPlaced) {
		this.isPlaced = isPlaced;
	}
	
	public boolean getIsPlaced() {
		return isPlaced;
	}
}
