package logic;

public class StateHandler {

    public static void stepUp(Level level) {
    	if(level.getPlayer().getYCoordinate() == level.getLevelHeight() - level.getPlayer().getStepDistance()) 
    		return; // �������� �� ����� �� �������� ������������
    	
    	for(Wall wall : level.getWallList()) {   // �������� �� ������� ����� ����� �������
    		if ((wall.getYCoordinate() - level.getPlayer().getYCoordinate() == level.getPlayer().getStepDistance()) && 
    				(wall.getXCoordinate() == level.getPlayer().getXCoordinate()))
    			return;
    	}	
    	
    	for(Box box : level.getBoxList()) {   // �������� �� ������� ������
    		if((box.getYCoordinate() - level.getPlayer().getYCoordinate() == level.getPlayer().getStepDistance()) && 
    				(box.getXCoordinate() == level.getPlayer().getXCoordinate())) {    			
    			for(Wall wall : level.getWallList()) {   // �������� �� ������� ����� �� �������
    				if((wall.getYCoordinate() - box.getYCoordinate()== level.getPlayer().getStepDistance()) && 
    						(wall.getXCoordinate() == box.getXCoordinate()))
    					return;
    			}
    			
    			for(Box box1 : level.getBoxList()) {   // �������� �� ������� ������ �� �������
    				if((box1.getYCoordinate() - box.getYCoordinate() == level.getPlayer().getStepDistance()) && 
    						(box1.getXCoordinate() == box.getXCoordinate()))
    					return;
    			}
    		}
    	}
    	
    	level.getPlayer().setPastCoordinates(); // ���������� ���������� ���������� ������
    	level.getPlayer().moveUp(); // �������� ������ � �������� �����������
    	level.increaseStepCount();
    	
    	for(Box box : level.getBoxList()) {   // �������� �� ������� ����� ����� �������
    		box.setPastCoordinates(); // ���������� ���������� ���� �������� �������� 
    		
    		// ��� ����������� �������� ������� ������������ ������ ��������� ������� �� ���������� ����
    		if((level.getPlayer().getYCoordinate() == box.getYCoordinate()) && 
    				(level.getPlayer().getXCoordinate() == box.getXCoordinate()))
    			box.moveUp();	
    	}
    }
    
    public static void stepDown(Level level) {
    	if(level.getPlayer().getYCoordinate() == 0)
    		return;
    	
    	for(Wall wall : level.getWallList()) {
    		if ((level.getPlayer().getYCoordinate() - wall.getYCoordinate() == level.getPlayer().getStepDistance()) && 
    				(level.getPlayer().getXCoordinate() == wall.getXCoordinate()))
    			return;
    	}
    	
    	for(Box box : level.getBoxList()) {
    		if((level.getPlayer().getYCoordinate() - box.getYCoordinate() == level.getPlayer().getStepDistance()) && 
    				(level.getPlayer().getXCoordinate() == box.getXCoordinate())) {    			
    			for(Wall wall : level.getWallList()) {
    				if((box.getYCoordinate() - wall.getYCoordinate() == level.getPlayer().getStepDistance()) && 
    						(box.getXCoordinate() == wall.getXCoordinate()))
    					return;
    			}
    			
    			for(Box box1 : level.getBoxList()) {
    				if((box.getYCoordinate() - box1.getYCoordinate() == level.getPlayer().getStepDistance()) && 
    						(box.getXCoordinate() == box1.getXCoordinate()))
    					return;
    			}
    		}
    	}
    	
    	level.getPlayer().setPastCoordinates();
    	level.getPlayer().moveDown();
    	level.increaseStepCount();
    		
    	for(Box box : level.getBoxList()) {
    		box.setPastCoordinates();
    		if((box.getYCoordinate() == level.getPlayer().getYCoordinate()) && 
    				(box.getXCoordinate() == level.getPlayer().getXCoordinate()))
    			box.moveDown();
    	}
    }
	
    public static void stepLeft(Level level) {
    	if(level.getPlayer().getXCoordinate() == 0)
    		return;
    	
    	for(Wall wall : level.getWallList()) {
    		if ((level.getPlayer().getXCoordinate() - wall.getXCoordinate() == level.getPlayer().getStepDistance()) && 
    				(wall.getYCoordinate() == level.getPlayer().getYCoordinate()))
    			return;
    	}	
    	
    	for(Box box : level.getBoxList()) {
    		if((level.getPlayer().getXCoordinate() - box.getXCoordinate() == level.getPlayer().getStepDistance()) && 
    				(box.getYCoordinate() == level.getPlayer().getYCoordinate())) {
    			for(Wall wall : level.getWallList()) {
    				if((box.getXCoordinate() - wall.getXCoordinate() == level.getPlayer().getStepDistance()) && 
    						(box.getYCoordinate() == wall.getYCoordinate()))
    					return;
    			}
    			
    			for(Box box1 : level.getBoxList()) {
    				if((box.getXCoordinate() - box1.getXCoordinate() == level.getPlayer().getStepDistance()) && 
    						(box.getYCoordinate() == box1.getYCoordinate()))
    					return;
    			}
    		}
    	}
    	
    	level.getPlayer().setPastCoordinates();
    	level.getPlayer().moveLeft();
    	level.increaseStepCount();
    		
    	for(Box box : level.getBoxList()) {
    		box.setPastCoordinates();
    		if((level.getPlayer().getXCoordinate() == box.getXCoordinate()) && 
    				(level.getPlayer().getYCoordinate() == box.getYCoordinate()))
    			box.moveLeft();
    	}
    }
    
    public static void stepRight(Level level) {
    	if(level.getPlayer().getXCoordinate() == level.getLevelWidth() - level.getPlayer().getStepDistance())
    		return;
    	
    	for(Wall wall : level.getWallList()) {
    		if ((wall.getXCoordinate() - level.getPlayer().getXCoordinate() == level.getPlayer().getStepDistance()) && 
    				(wall.getYCoordinate() == level.getPlayer().getYCoordinate()))
    			return;
    	}
    	
    	for(Box box : level.getBoxList()) {
    		if((box.getXCoordinate() - level.getPlayer().getXCoordinate() == level.getPlayer().getStepDistance()) && 
    				(box.getYCoordinate() == level.getPlayer().getYCoordinate())) {    			
    			for(Wall wall : level.getWallList()) {
    				if((wall.getXCoordinate() - box.getXCoordinate() == level.getPlayer().getStepDistance()) && 
    						(box.getYCoordinate() == wall.getYCoordinate()))
    					return;
    			}
    			
    			for(Box box1 : level.getBoxList()) {
    				if((box1.getXCoordinate() - box.getXCoordinate() == level.getPlayer().getStepDistance()) && 
    						(box.getYCoordinate() == box1.getYCoordinate()))
    					return;
    			}
    		}
    	}
    	
    	level.getPlayer().setPastCoordinates();
    	level.getPlayer().moveRight();
    	level.increaseStepCount();
    		
    	for(Box box : level.getBoxList()) {
    		box.setPastCoordinates();
    		if((box.getXCoordinate() == level.getPlayer().getXCoordinate()) && 
    				(level.getPlayer().getYCoordinate() == box.getYCoordinate()))
    			box.moveRight();
    	}
    }
    
    public static void stepUndo(Level level) {
    	level.getPlayer().moveUndo();
    	
    	for(Box box : level.getBoxList())
    		box.moveUndo();
    }
    
    //�������� �� ������������ ������ � �������
    public static void checkBoxesPlaced(Level level) { 
    	for(Box box: level.getBoxList()) {
			box.setIsPlaced(false);
		}
    	
		for(Box box : level.getBoxList()) {
			for(EndZone endZone : level.getEndZoneList()) {
				if((box.getXCoordinate() == endZone.getXCoordinate()) && 
						(box.getYCoordinate() == endZone.getYCoordinate())) {
					box.setIsPlaced(true);
				}
			}
		}
    }
	
    //�������� �� ����������� ������
	public static void checkIsCompleted(Level level) {
		level.setIsCompleted(true);
		
		for(Box box : level.getBoxList()) {
			if (box.getIsPlaced() == false) {
				level.setIsCompleted(false);
			}
		}	
	}
}
