package logic;

public abstract class MovableEntity extends Entity {
	private static final long serialVersionUID = -693878554597098877L;
	
	protected int pastXCoordinate;
	protected int pastYCoordinate;
	
	protected final int stepDistance = 1; // ����� � ������ ���������� �� 1 ������� �� ���
	
	public void setPastXCoordinate(int x) {
		this.pastXCoordinate = x;
	}
	
	public int getPastXCoordinate() {
		return pastXCoordinate;
	}
	
	public void setPastYCoordinate(int y) {
		this.pastYCoordinate = y;
	}
	
	public int getPastYCoordinate() {
		return pastYCoordinate;
	}
	
	public void setPastCoordinates() {
		setPastXCoordinate(xCoordinate);
		setPastYCoordinate(yCoordinate);
	}
	
	public void moveUp() {
		setYCoordinate(getYCoordinate()+stepDistance);
	}
	
	public void moveDown() {
		setYCoordinate(getYCoordinate()-stepDistance);
	}
	
	public void moveLeft() {
		setXCoordinate(getXCoordinate()-stepDistance);
	}
	
	public void moveRight() {
		setXCoordinate(getXCoordinate()+stepDistance);
	}
	
	public void moveUndo() {
		if(xCoordinate != pastXCoordinate)
			setXCoordinate(pastXCoordinate);
		else 
			setYCoordinate(pastYCoordinate);
	}
	
	public int getStepDistance() {
		return stepDistance;
	}
}
