package logic;

public class Wall extends FixedEntity {
	private static final long serialVersionUID = -1208536276267042106L;

	public Wall(int x, int y) {
		this.xCoordinate = x;
		this.yCoordinate = y;
	}
}
