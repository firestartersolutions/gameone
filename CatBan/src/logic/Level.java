package logic;

import java.io.Serializable;
import java.util.ArrayList;

public class Level implements Serializable {
	private static final long serialVersionUID = 3730414253517779770L;
	
	private String levelName; // ��� ������
	
	private Player player;
	
	private ArrayList<Wall> wallList = new ArrayList<Wall>();
	private ArrayList<Box> boxList = new ArrayList<Box>();
	private ArrayList<EndZone> endZoneList = new ArrayList<EndZone>();
	private ArrayList<Wall> floorList = new ArrayList<Wall>();
	private int levelWidth; // ������ ������ � �����������
	private int levelHeight; // ������ ������ � �����������
	
	private boolean isCompleted; // ������������� ���������� ������
	
	private int stepCount; // ������� ���������� �����
	private int goodStepCount; // ���������� ����� �� 2 ������
	private int perfectStepCount; // ���������� ����� �� 3 ������
	
	public Level(String name, int levelWidth, int levelHeight, int perfectStepCount, int goodStepCount) {
		this.levelName = name;
        this.levelWidth = levelWidth;
        this.levelHeight = levelHeight;
        this.goodStepCount = goodStepCount;
        this.perfectStepCount = perfectStepCount;
        this.isCompleted = false;
        this.stepCount = 0;
	}
	
	public void setLevelName(String name) {
		this.levelName = name;
	}
	
	public String getLevelName() {
		return levelName;
	}
	
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public void setWallList(ArrayList<Wall> wallList) {
		this.wallList = wallList;
	}
	
	public ArrayList<Wall> getWallList() {
		return wallList;
	}
	
	public void setFloorList(ArrayList<Wall> floorList) {
		this.floorList = floorList;
	}
	
	public ArrayList<Wall> getFloorList() {
		return floorList;
	}
	
	public void setBoxList(ArrayList<Box> boxList) {
		this.boxList = boxList;
	}
	
	public ArrayList<Box> getBoxList() {
		return boxList;
	}
	
	public void setEndZoneList(ArrayList<EndZone> endZoneList) {
		this.endZoneList = endZoneList;
	}
	
	public ArrayList<EndZone> getEndZoneList() {
		return endZoneList;
	}
	
	public int getLevelWidth() {
		return levelWidth;
	}
	
	public int getLevelHeight() {
		return levelHeight;
	}
	
	public void setIsCompleted(boolean isCompleted) {
		this.isCompleted = isCompleted;
	}
	
	public boolean getIsCompleted() {
		return isCompleted;
	}
	
	public void increaseStepCount() {
		stepCount++;
	}
	
	public void setGoodStepCount(int value) {
		this.goodStepCount = value;
	}

	public int getGoodStepCount() {
		return goodStepCount;
	}
	
	public void setPerfectStepCount(int value) {
		this.perfectStepCount = value;
	}
	
	public int getPerfectStepCount() {
		return perfectStepCount;
	}
	
	public void setStepCount(int stepCount) {
		this.stepCount = stepCount;
	}
	
	public int getStepCount() {
		return stepCount;
	}
}
